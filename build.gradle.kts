plugins {
  kotlin("jvm") version "1.8.21"
  `java-gradle-plugin`
  `maven-publish`
  id("com.gradle.plugin-publish") version "1.2.0"
}

repositories {
  mavenCentral()
  google()
}

group = "com.gitlab.edyjen"
version = "0.1.4"


dependencies {
  compileOnly("com.android.tools.build:gradle:8.0.2")
}

gradlePlugin {
  website.set("https://gitlab.com/edyjen/android-versioning")
  vcsUrl.set("https://gitlab.com/edyjen/android-versioning.git")

  plugins {
    create("versioningPlugin") {
      id = "com.gitlab.edyjen.android-versioning"
      displayName = "Android Versioning Gradle Plugin"
      description = "Gradle plugin to automatically generate Android versionName and versionCode using Git."
      tags.set(listOf("versioning", "android", "artifact", "version"))
      implementationClass = "edyjen.gradle.VersioningPlugin"
    }
  }
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      from(components["java"])
      pom {
        name.set(project.name)
        description.set("Android Versioning Gradle Plugin using Git")
        url.set("https://gitlab.com/edyjen/android-versioning")
        licenses {
          license {
            name.set("The Apache Software License, Version 2.0")
            url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
          }
        }
        developers {
          developer {
            id.set("pedinel")
            name.set("Pedro Parra")
            email.set("pedromiguelp18@gmail.com")
          }
        }
        scm {
          connection.set("https://gitlab.com/edyjen/android-versioning.git")
          developerConnection.set("https://gitlab.com/edyjen/android-versioning.git")
          url.set("https://gitlab.com/edyjen/android-versioning")
        }
      }
    }
  }
  repositories {
    maven {
      name = "localPluginRepository"
      url = uri("../local-plugin-repository")
    }
  }
}

//publish task: publishPlugins --validate-only
