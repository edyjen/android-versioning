/*
 * Created by Pedro Parra <pedromiguelp18@gmail.com> on 06-06-2023.
 * Copyright © 2023 Pedinel. All rights reserved.
 */

package edyjen.gradle

import edyjen.gradle.utils.executeCommand

class Versioning(private val ext: VersioningPluginExtension) {
  internal val branchName by lazy {
    "git rev-parse --abbrev-ref HEAD".executeCommand()
  }

  internal val versionCode by lazy {
    var ref = ext.baseBranch
    if (!isBaseBranch) ref += ".."
    "git rev-list --first-parent --count $ref".executeCommand().toInt()
  }

  internal val versionCodeWithOffset by lazy {
    versionCode + ext.versionOffset
  }

  private val tagDescription by lazy {
    "git describe --tags --dirty --match \"v[0-9]*.[0-9]*\"".executeCommand().let {
      if (it.startsWith("v")) it.substring(1)
      else it
    }.split("-".toRegex(), 2)
  }

  internal val latestTagVersions by lazy {
    val version = tagDescription.first().split(".").toMutableList()
    while (version.size < 3) {
      version.add("0")
    }
    version
  }

  private val commitRef by lazy {
    if (tagDescription.size < 2) return@lazy null
    tagDescription.last()
  }

  internal val commitsAheadCount by lazy {
    val ref = commitRef ?: return@lazy 0
    if (!ref.contains('-')) return@lazy 0

    ref.substringBefore('-').toInt()
  }

  internal val isBaseBranch by lazy {
    branchName == ext.baseBranch
  }

  internal val versionToUse by lazy {
    val versions = latestTagVersions

    if (isBaseBranch && ext.autoIncreaseFixesVersion) {
      var fixes = commitsAheadCount
      if (ext.useNextFixesVersion && fixes > 0) fixes = 1
      versions[versions.size - 1] = (versions.last().toInt() + fixes).toString()
    }

    versions.joinToString(".")
  }

  internal val versionName by lazy {
    var v = versionToUse

    if (ext.includeVersionCodeSuffix) {
      v += ".$versionCodeWithOffset"
    }

    if (!isBaseBranch) {
      if (ext.includeBranchNameOnAltBranches) {
        v += "-$branchName".replace("/", "_")
      }

      if (ext.includeChangesCountOnAltBranches && commitsAheadCount > 0) {
        v += "+$commitsAheadCount"
      }
    }

    if (commitRef?.contains("dirty") == true) {
      if ((isBaseBranch && ext.includeLocalChangesOnBaseBranch) || (!isBaseBranch && ext.includeLocalChangesOnAltBranches)) {
        val stats = mutableListOf("0", "0", "0")
        "git diff --shortstat".executeCommand().split(", ").forEach {
          val index = when {
            it.contains("insertion") -> 1
            it.contains("deletion") -> 2
            else -> 0
          }
          stats[index] = it.substringBefore(" ")
        }

        v += "(${stats[0]},+${stats[1]},-${stats[2]})".replace("(0,+0,-0)", "")
      }
    }

    v
  }
}