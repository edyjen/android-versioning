/*
 * Created by Pedro Parra <pedromiguelp18@gmail.com> on 06-06-2023.
 * Copyright © 2023 Pedinel. All rights reserved.
 */

package edyjen.gradle

open class VersioningPluginExtension {

  var baseBranch = "master"
  var includeLocalChangesOnBaseBranch = false
  var includeLocalChangesOnAltBranches = true
  var includeVersionCodeSuffix = true
  var includeBranchNameOnAltBranches = true
  var autoIncreaseFixesVersion = true
  var useNextFixesVersion = true
  var includeChangesCountOnAltBranches = true
  var versionOffset = 0
  var isApkRenamingEnabled = true

  private val versioning by lazy { Versioning(this) }
  val versionCode by lazy { versioning.versionCodeWithOffset }
  val versionName by lazy { versioning.versionName }

  val version by lazy {
    versioning.latestTagVersions.joinToString(".")
  }

  val changesCount by lazy {
    versioning.versionCode
  }
}