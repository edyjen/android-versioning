/*
 * Created by Pedro Parra <pedromiguelp18@gmail.com> on 06-06-2023.
 * Copyright © 2023 Pedinel. All rights reserved.
 */

package edyjen.gradle

import com.android.build.gradle.AppExtension
import com.android.build.gradle.internal.api.BaseVariantOutputImpl
import edyjen.gradle.utils.generateOutputName
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePluginExtension

@ExperimentalStdlibApi
class VersioningPlugin : Plugin<Project> {
  override fun apply(project: Project) {
    with(project) {
      val ext = extensions.create("versioning", VersioningPluginExtension::class.java)

      tasks.register("printVersions") {
        it.group = "Versioning"
        it.description = "Prints the Android version information."
        it.doLast {
          ext.versionName
          ext.versionCode
        }
      }

      pluginManager.withPlugin("com.android.application") {
        val appExt = extensions.getByType(AppExtension::class.java)

        appExt.applicationVariants.configureEach { variant ->
          if (ext.isApkRenamingEnabled) {
            val baseName = extensions.findByType(BasePluginExtension::class.java)?.archivesName?.get() ?: name
            variant.outputs.configureEach {
              val extension = (it as BaseVariantOutputImpl).outputFileName.substring(it.outputFileName.lastIndexOf('.'))
              it.outputFileName = variant.generateOutputName(baseName, extension)
            }
          }
        }
      }
    }
  }
}