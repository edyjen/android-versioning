/*
 * Created by Pedro Parra <pedromiguelp18@gmail.com> on 06-06-2023.
 * Copyright © 2023 Pedinel. All rights reserved.
 */

package edyjen.gradle.utils

import com.android.build.gradle.api.ApplicationVariant
import java.io.File

internal fun ApplicationVariant.generateOutputName(baseName: String, extension: String): String {
  return StringBuilder().apply {
    append(baseName)
    productFlavors.forEach {
      append("-")
      append(it.name)
    }
    append("-")
    append(versionName)
    append("-")
    append(buildType.name)
    append(extension)
  }.toString()
}