/*
 * Created by Pedro Parra <pedromiguelp18@gmail.com> on 06-06-2023.
 * Copyright © 2023 Pedinel. All rights reserved.
 */

package edyjen.gradle.utils

import org.gradle.api.GradleException
import java.util.Locale
import java.util.concurrent.TimeUnit

internal fun String.executeCommand(): String {
  return try {
    ProcessBuilder(this.split("\\s".toRegex())).start().run {
      waitFor(10, TimeUnit.SECONDS)
      inputStream.bufferedReader().use { it.readText().trim() }
    }
  } catch (e: Exception) {
    throw GradleException(e.message ?: "Unable to execute: $this")
  }
}